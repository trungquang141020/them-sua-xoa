<?php 
	namespace App\Http\Controllers;
	/**
	 * 
	 */
	//gọi file  model user
	use App\Models\user;
	use Illuminate\Http\Request;
	class UserController extends Controller
	{
		
		public function getlist(){
			//user của model
			$user=user::paginate(5);
			return view('list',[
				'us'=>$user //us biến để lưu dữ liệu của $user
				]);
			
		}
		public function getedit($id){
			$use=user::find($id);
			return view('edit',[
				'use'=>$use
			]);	
		}
		public function postedit($id,Request $rq){
				$rq->offsetUnset('_token');
				user::where(['id'=>$id])->update($rq->all());
				return redirect()->route('getlist');
		}
		public function getadd(){
			return view('add');
		}
		public function postadd(Request $rq){
			//user của model
			//$rq biến lưu gt post
			user::create([
				'name'=>$rq->name,
				'email'=>$rq->email,
				'pass'=>MD5($rq->pass),

			]);
			return redirect()->route('getlist');
		}
		public function getdel($id){
			user::find($id)->delete();
			 return redirect()->back();//redirect();chuyển hướng /chuyển hướng về trang cũ
		}
	}

 ?>