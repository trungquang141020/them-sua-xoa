<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/' ,function () {
    return view('home');
});
Route::get('/category', function () {
    return view('category');
});
Route::get('/app', function () {
    return view('layouts.app');
});
Route::group(['prefix'=>'user'],function (){
	//đường dẫn đến form list
	Route::get('list','UserController@getlist')->name('getlist');
	//đường dãn tới submit thêm
	//phương thức get dùng để tạo view và hiển thị dữ liệu
	Route::get('add','UserController@getadd')->name('getadd');
	Route::post('add','UserController@postadd')->name('postadd');
	//đường dẫn tới submit sửa
	//phuonwng thức post dùng để lấy dữ liệu kkhi submit và lưu vào csdl
	Route::get('edit/{id}','UserController@getedit')->name('getedit');
	Route::post('edit/{id}','UserController@postedit')->name('postedit');
	//đường dẫn tới xóa
	Route::get('del/{id}','UserController@getdel')->name('getdel');
});
Route::get('/contact', function () {
    return view('contact');
});
Route::post('/contact/send', function () {
    return view('send.php');
});