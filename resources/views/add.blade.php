@extends('home')
@section('content')
@include('sidebar')
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">Panel title</h3>
	</div>
	<div class="panel-body">
		<form action="" method="POST" role="form">
			@csrf
			<div class="form-group">
				<label for="">Tên</label>
				<input type="text" class="form-control"  name="name" id="" placeholder="Input field" ">
			</div>
			<div class="form-group">
				<label for="">Email</label>
				<input type="text" class="form-control"  name="email" id="" placeholder="Input field" >
			</div>
			<div class="form-group">
				<label for="">Password</label>
				<input type="text" class="form-control"  name="pass" id="" placeholder="Input field" >
			</div>
			
			<button type="submit" class="btn btn-primary">Submit</button>
		</form>
	</div>
</div>
</div>
@endsection