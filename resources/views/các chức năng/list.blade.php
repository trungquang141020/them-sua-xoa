@extends('home')
@section('content')
@include('sidebar')
<div class="panel panel-primary">
	<!-- Default panel contents -->
	<div class="panel-heading">Panel heading</div>
	<div class="panel-body">
		
	</div>
	<table class="table">
		<thead>
			<tr>
				<th>iD</th>
				<th>Name</th>
				<th>Email</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($us as $u) 

			<tr>
				<td>{{ $u ->id}}</td>
				<td>{{$u->name}}</td>
				<td>{{$u->email}}</td>
				<td><a href="{{ route ('getedit',['id'=>$u->id] )}}" class="btn btn-sm btn-success">Sửa</a>
					<a href="{{ route ('getdel',['id'=>$u->id] )}}" class="btn btn-sm btn-danger" onclick="return confirm(bạn có chắc chắn muốn xóa user này">Xóa</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
<div class="clearfix">
	{{$us->links()}}
</div>
</div>

@endsection
